import React, { Component }  from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from 'react-router-dom';
import Axios from 'axios';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControlLabel } from '@material-ui/core';
import Favorite from '@material-ui/icons/Favorite';
import ReactPaginate from 'react-paginate';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import MicIcon from '@material-ui/icons/Mic';
import './style.css'

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const recognition = new SpeechRecognition()

recognition.continous = true
recognition.interimResults = true
recognition.lang = 'fr-FR'
export default class Ouvrage extends Component {
    constructor(props) {
        super(props)
        this.searchHandler=this.searchHandler.bind(this); 
        this.handleChange=this.handleChange.bind(this); 
        this.toggleListen = this.toggleListen.bind(this)
        this.handleListen = this.handleListen.bind(this)
        
         this.state={
          ouvrages:[],
          message:'',
          nombre:'',
          checked:'',
          offset: 0,
          perPage: 10,
          currentPage: 0,
          listening: false,
          finalTranscript:'',
          bgColor: '#4b63db'
         
        };
    
        }
        toggleListen() {
          this.setState({
            bgColor: '#e82a33',
            listening: !this.state.listening
          }, this.handleListen)
        }
        
        handleListen(){
          if (this.state.listening) {
              recognition.start()
        
            let finalTranscript = ''
            recognition.onresult = event => {
              let interimTranscript = ''
        
              for (let i = event.resultIndex; i < event.results.length; i++) {
                const transcript = event.results[i][0].transcript;
                if (event.results[i].isFinal) finalTranscript += transcript + ' ';
                else interimTranscript += transcript;
              }
              this.setState({finalTranscript:finalTranscript});
            if(this.state.finalTranscript!=''){
              this.setState({bgColor:'#4b63db'});
                this.searchHandler1(this.state.finalTranscript)
            }
          }
        }
        else{
          recognition.stop()
          alert('Une erreur est survenue!')
         
        }
      }
        handlePageClick = (e) => {
          const selectedPage = e.selected;
          const offset = selectedPage * this.state.perPage;
    
          this.setState({
              currentPage: selectedPage,
              offset: offset
          }, () => {
              this.componentDidMount()
          });
    
      };
        handleChange(e,id){
         // const { checked } = this.state;
        //  this.setState({checked:[e.target.checked]})
        if(e.target.checked){
          const favori={
            idMembre:sessionStorage.getItem("idMembre"),
            idOuvrage:id,
            checked:'true',
            
          }
          console.log(favori)
         
          Axios.post(' https://phpapiserver.herokuapp.com/api/favori/check.php/?idMembre='+sessionStorage.getItem("idMembre")+'&idOuvrage='+id)
          .then(res=>{
           if(res.data.message=='Could not find data.'){
            console.log('CNF')
            Axios.post('https://phpapiserver.herokuapp.com/api/favori/create.php',favori)
            .then(res=>{
              console.log(res.data)
              //console.log(e.target.name)
            });
            //this.setState({checked:e.target.name})
           }
           else{
            console.log('Hita')
           }
          })
          .catch(error=>{
           
         }) 
          //condition if miexist d ts apidirina sinon oui apdirina
     
         
        }
        else{
          const favori={
            idMembre:sessionStorage.getItem("idMembre"),
            idOuvrage:id,
            
          }
          console.log(favori)
        Axios.post('https://phpapiserver.herokuapp.com/api/favori/deleteOne.php',favori)
          .then(res=>{
            console.log(res.data)
            //console.log(e.target.name)
          });
         // this.setState({checked:''}) 
          console.log('unchecked=');
        }
          
               

          /*if(e.target.checked){
            console.log('checked='+e.target.name)
            this.setState({checked:e.target.checked})
          }
          else{
            console.log('unchecked')
            this.setState({checked:false})
          }*/
         
         
        }
        componentDidMount(){
            Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrage.php')
            .then(response=>{
              const data = response.data;
			        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
			        const postdata = slice.map(ouvrage=>
              <div className="col-sm-12 col-lg-12">
                        
              <div className="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
              <img  className="mr-4" src={decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} width="100px" style={{"borderRadius":"5px"}}/>
            <div className="media-body" style={{"textAlign":"justify"}}>
                <h5><Link style={{"color":"#1d1e52"}}  to={`/detailOuvrage/${ouvrage.idOuvrage}`}>{ouvrage.titre} ({ouvrage.nomAuteur})</Link></h5>
                {ouvrage.description}
               <br/> <i> par {ouvrage.nomAuteur}|{ouvrage.nomGenre}| {ouvrage.nombrePage} pages| {ouvrage.descriptionGenre}</i>
              <br/> 
                <div style={{display: "inline-block"}}>
                {(()=>{ 
                   if(sessionStorage.getItem("idMembre")!=null){
                  //Axios.post('https://phpapiserver.herokuapp.com/api/favori/check.php/?idMembre='+sessionStorage.getItem("idMembre")+'&idOuvrage='+ouvrage.idOuvrage)
                  //.then(res=>{
                  
                    return( <FormControlLabel   className="checkbox"
                    control={<Checkbox   onChange={(e) => this.handleChange(e, ouvrage.idOuvrage)} icon={<FavoriteBorder style={{color:'#eb1552'}}/>}  checkedIcon={<Favorite style={{color:'#eb1552'}} />} name={`checked${ouvrage.idOuvrage}`} />}
                    label=""
                  />)
                //})
                }
                })()}
                     <Link  style={{"color":"#1d1e52"}} to={`detailOuvrage/${ouvrage.idOuvrage}`}> 
                    Voir les commentaires
                  </Link>
                </div>
            </div>
        </div>
    </div>)
     this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
      
              
            })
            .catch(error=>{
              console.log(error)
            })
        }
        searchHandler1(e){
     
          
            var test=' résultat(s) trouvé(s)'
          Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/search.php',{wordToSearch:e})
          .then(response=>{
            if(response.data.message!='Could not find data.'){
              const data = response.data;
              const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
              const postdata = slice.map(ouvrage=>
              <div className="col-sm-12 col-lg-12">
                        
              <div className="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
              <img  className="mr-4" src={decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} width="100px" style={{"borderRadius":"5px"}}/>
            <div className="media-body" style={{"textAlign":"justify"}}>
                <h5><Link style={{"color":"#1d1e52"}}  to={`/detailOuvrage/${ouvrage.idOuvrage}`}>{ouvrage.titre} ({ouvrage.nomAuteur})</Link></h5>
                {ouvrage.description}
               <br/> <i> par {ouvrage.nomAuteur}|{ouvrage.nomGenre}| {ouvrage.nombrePage} pages| {ouvrage.descriptionGenre}</i>
              <br/> 
                <div style={{display: "inline-block"}}>
                {(()=>{ 
                   if(sessionStorage.getItem("idMembre")!=null){
                  //Axios.post('https://phpapiserver.herokuapp.com/api/favori/check.php/?idMembre='+sessionStorage.getItem("idMembre")+'&idOuvrage='+ouvrage.idOuvrage)
                  //.then(res=>{
                  
                    return( <FormControlLabel   className="checkbox"
                    control={<Checkbox   onChange={(e) => this.handleChange(e, ouvrage.idOuvrage)} icon={<FavoriteBorder style={{color:'#eb1552'}}/>}  checkedIcon={<Favorite style={{color:'#eb1552'}} />} name={`checked${ouvrage.idOuvrage}`} />}
                    label=""
                  />)
                //})
                }
                })()}
                     <Link  style={{"color":"#1d1e52"}} to={`detailOuvrage/${ouvrage.idOuvrage}`}> 
                    Voir les commentaires
                  </Link>
                </div>
            </div>
        </div>
    </div>)
             
             this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
            }
            else{
              this.setState({message:'Aucun'+test,ouvrages:[]})
            }
           
            
          
            //this.setState({posts:response.data})
          })
        }
          
        searchHandler(e){
          this.setState({finalTranscript:e.target.value})
            console.log("search="+e.target.value)
            if(e.target.value==''){
             this.componentDidMount()
            }
            else{
      
              var test=' résultat(s) trouvé(s)'
            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/search.php',{wordToSearch:e.target.value})
            .then(response=>{
              if(response.data.message!='Could not find data.'){
                const data = response.data;
                const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
                const postdata = slice.map(ouvrage=>
                <div className="col-sm-12 col-lg-12">
                          
                <div className="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                <img  className="mr-4" src={decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} width="100px" style={{"borderRadius":"5px"}}/>
              <div className="media-body" style={{"textAlign":"justify"}}>
                  <h5><Link style={{"color":"#1d1e52"}}  to={`/detailOuvrage/${ouvrage.idOuvrage}`}>{ouvrage.titre} ({ouvrage.nomAuteur})</Link></h5>
                  {ouvrage.description}
                 <br/> <i> par {ouvrage.nomAuteur}|{ouvrage.nomGenre}| {ouvrage.nombrePage} pages| {ouvrage.descriptionGenre}</i>
                <br/> 
                  <div style={{display: "inline-block"}}>
                  {(()=>{ 
                     if(sessionStorage.getItem("idMembre")!=null){
                    //Axios.post('https://phpapiserver.herokuapp.com/api/favori/check.php/?idMembre='+sessionStorage.getItem("idMembre")+'&idOuvrage='+ouvrage.idOuvrage)
                    //.then(res=>{
                    
                      return( <FormControlLabel   className="checkbox"
                      control={<Checkbox   onChange={(e) => this.handleChange(e, ouvrage.idOuvrage)} icon={<FavoriteBorder style={{color:'#eb1552'}}/>}  checkedIcon={<Favorite style={{color:'#eb1552'}} />} name={`checked${ouvrage.idOuvrage}`} />}
                      label=""
                    />)
                  //})
                  }
                  })()}
                       <Link  style={{"color":"#1d1e52"}} to={`detailOuvrage/${ouvrage.idOuvrage}`}> 
                      Voir les commentaires
                    </Link>
                  </div>
              </div>
          </div>
      </div>)
               
               this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
              }
              else{
                this.setState({message:'Aucun'+test,ouvrages:[]})
              }
             
              
            
              //this.setState({posts:response.data})
            })
          }
            }
   render(){
    return (
        <section className="services">
        <div className="container">
        <div className="row">
            <div className="col-sm-12 col-lg-12"><br/><br/><br/></div>
        </div>
            <div className="title text-center">
               <br/>
               <h1 className="title-blue">LES OUVRAGES</h1>
            </div>
            <div className="container">
            <div className="row">
            <div className="col-sm-3 col-lg-3"></div>
            <div className="col-sm-6 col-lg-6">
                    <form  noValidate autoComplete="off">
                       
                        <TextField
                            id="filled-secondary"
                            label="Rechercher un ouvrage"
                            variant="filled"
                            color="secondary"
                            onChange={this.searchHandler}
                            value={this.state.finalTranscript}
                            fullWidth
                            InputProps={{
                                endAdornment: (
                                  <InputAdornment >
                                    <IconButton onClick={this.toggleListen}>
                                      <MicIcon style={{"color":this.state.bgColor}}/>
                                    </IconButton>
                                    <IconButton>
                                      <SearchIcon style={{"color":"grey"}}/>
                                    </IconButton>
                                  </InputAdornment>
                                )
                              }}
                        />
                       
                    </form>
                    </div> 
                    <div className="col-sm-3 col-lg-3"></div>
            </div>
                <div className="row">
                <span style={{color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </span>
                {this.state.postdata}  
             
             <div style={{marginLeft:'40%'}}>
             <ReactPaginate 
                   previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                   nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                   breakLabel={"..."}
                   breakClassName={"break-me"}
                   pageCount={this.state.pageCount}
                   marginPagesDisplayed={2}
                   pageRangeDisplayed={5}
                   onPageChange={this.handlePageClick}
                   containerClassName={"pagination"}
                   subContainerClassName={"pages pagination"}
                   activeClassName={"active"}/>
            </div>
                     
                </div>
            </div>
        </div>
    </section>
    
    )
   }
}