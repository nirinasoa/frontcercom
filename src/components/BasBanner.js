import React, { Component }  from 'react';
import {Link} from 'react-router-dom';

export default class BasBanner extends Component {
   render(){
    let banner;
    if(sessionStorage.getItem("idMembre")!=null){
        banner=    <div class="cta-content d-xl-flex align-items-center justify-content-around text-center text-xl-left">
        <div class="content" data-aos="fade-right" data-aos-delay="200">
            <h2>LES OUVRAGES LES PLUS LUS</h2>
            <p>Pour voir les ouvrages les plus lus, veuillez cliquer sur le bouton en bas.</p>
        </div>
        <div class="subscribe-btn" data-aos="fade-left" data-aos-delay="400" data-aos-offset="0">
            <Link to="/progressionLecture" class="btn btn-primary">Voir</Link>
        </div>
    </div>
    }
    if(sessionStorage.getItem("idMembre")==null){
        banner=  <div class="cta-content d-xl-flex align-items-center justify-content-around text-center text-xl-left">
        <div class="content" data-aos="fade-right" data-aos-delay="200">
            <h2>LES OUVRAGES LES PLUS LUS</h2>
            <p>Pour voir les ouvrages les plus lus, veuillez d'abord vous connecter.</p>
        </div>
        <div class="subscribe-btn" data-aos="fade-left" data-aos-delay="400" data-aos-offset="0">
            <Link to="/login" class="btn btn-primary">Se connecter</Link>
        </div>
    </div>
    }
    return (
        <section class="cta" data-aos="fade-up" data-aos-delay="0">
        <div class="container">
           {banner}
        </div>
    </section>
    
    )
   }
}