import React, { Component }  from 'react';
import TextField from '@material-ui/core/TextField';
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { Link } from 'react-router-dom';
import Axios from 'axios';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Checkbox from '@material-ui/core/Checkbox';
import { FormControlLabel } from '@material-ui/core';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteIcon from '@material-ui/icons/Favorite';
import './style.css'
export default class Favori extends Component {
    constructor(props) {
        super(props)
            this.state={
            favoris:[],
            message:'',
            nombre:'',
            checked:'',
            
            
            };
    
        }
        componentDidMount(){
            Axios.get('https://phpapiserver.herokuapp.com/api/favori/nombreFavoriParMembre.php/?idMembre='+sessionStorage.getItem("idMembre"))
            .then(response=>{
                if(response){
                   
                    this.setState({nombre:response.data.nombre})
                }
                else{
                    this.setState({nombre:'Aucun'})
                }
              
            })
            .catch(error=>{
                this.setState({nombre:'Aucun'})
            })
            Axios.get('https://phpapiserver.herokuapp.com/api/favori/ficheFavoriOk.php/?idMembre='+sessionStorage.getItem("idMembre"))
            .then(response=>{
                if(response.data.length>0){
                    console.log("ato="+response.data)
                    this.setState({favoris:response.data})
                }
                else{
                    this.setState({message:'Aucun favori',favoris:[]})
                }
              
            })
            .catch(error=>{
              console.log(error)
            })
        }
      
   render(){
    return (
        <section className="services">
        <div className="container">
        <div className="row">
            <div className="col-sm-12 col-lg-12"><br/><br/><br/></div>
        </div>
            <div className="title text-center">
               <br/>
               <h1 className="title-blue">MES FAVORIS</h1>
               
               <h2 style={{color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </h2>
            </div>
            <div className="container">
            <div className="row">
            <div className="col-sm-3 col-lg-3"></div>
           
                    <div className="col-sm-3 col-lg-3"></div>
            </div>
                <div className="row">
                <span style={{color:'#d6135e',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>
                    Nombre des ouvrages préférés: {this.state.nombre}
                </span>
               
                { this.state.favoris.map(ouvrage=>
                    <div className="col-sm-12 col-lg-10">
                        
                        <div className="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                            <img  className="mr-3" src={decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} width="100px" style={{"borderRadius":"5px"}}/>
                            <div className="media-body" style={{"textAlign":"justify"}}>
                                <h5><Link style={{"color":"#1d1e52",fontFamily:'Arial',fontSize:'18px'}}  to={`/detailOuvrage/${ouvrage.idOuvrage}`}>
                                    {ouvrage.titre} </Link>
                                </h5>
                              
                                 Auteur: {ouvrage.nomAuteur} <br/>Genre: {ouvrage.nomGenre}<br/>
                                 Langue: {ouvrage.langue}
                                 <br/>
                                 {(() => {
                                   
                                   if(ouvrage.nombre=="1"){
                                        return (<FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>);
                                    }
                                    else if(ouvrage.nombre=="2"){
                                        return (
                                            <div style={{display:'inline-block'}}>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small" />
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                            </div>
                                       
                                        
                                        );
                                    }
                                    else if(ouvrage.nombre=="3"){
                                        return (
                                            <div style={{display:'inline-block'}}>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small" />
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                            </div>
                                       
                                        
                                        );
                                    }
                                    else if(ouvrage.nombre=="4"){
                                        return (
                                            <div style={{display:'inline-block'}}>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small" />
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                            </div>
                                       
                                        
                                        );
                                    }
                                    else if(ouvrage.nombre=="5"){
                                        return (
                                            <div style={{display:'inline-block'}}>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small" />
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                            </div>
                                       
                                        
                                        );
                                    }
                                    else{
                                        return (
                                            <div style={{display:'inline-block'}}>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small" />
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <FavoriteIcon style={{color:'#f04374'}} fontSize="small"/>
                                                 <span style={{color:'#f04374'}}>+</span>
                                            </div>
                                       
                                        
                                        ); 
                                    }
                                   
                                })()}
                              <br/> 
                               
                            </div>
                        </div>
                    </div>
                )}   
                   
                     
                </div>
            </div>
        </div>
    </section>
    
    )
   }
}