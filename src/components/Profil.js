import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Axios from 'axios';
import { findAllByDisplayValue } from '@testing-library/react';

export default class Profil extends Component {
    constructor(props) {
        super(props)
        this.onclick=this.onclick.bind(this)
        this.modifier=this.modifier.bind(this)
        this.annuler=this.annuler.bind(this)
        this.envoyer=this.envoyer.bind(this)
        this.onChangeMotif=this.onChangeMotif.bind(this)
        this.onChangenom=this.onChangenom.bind(this)
        this.onChangeprenom=this.onChangeprenom.bind(this)
        this.onChangeadresse=this.onChangeadresse.bind(this)
        this.onChangefiliere=this.onChangefiliere.bind(this)
        this.onChangecin=this.onChangecin.bind(this)
        this.onChangedelivreA=this.onChangedelivreA.bind(this)
        this.onChangetelephone=this.onChangetelephone.bind(this)
        this.onChangedomaine=this.onChangedomaine.bind(this)
        this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
        this.onChangeFile = this.onChangeFile.bind(this)
        this.onChangeemail = this.onChangeemail.bind(this)
        this.enregistrer = this.enregistrer.bind(this)
        this.state={
            idmembre:sessionStorage.getItem("idMembre"),
            nom:'',
	        prenom:'',
            adresse:'',
            filiere:'',
            cin:'',
            delivreA:'',
            telephone:'',
            domaine:'',
            email:'',
            dateNaissance:'',
            photo:'',
            aitAcces:'',
            openDemande:false,
            motif:'',
            openModif:false
         
          };
        
        }
        enregistrer(){
            if(this.state.filename==null){
                const amodifierMembre={
                  idMembre:sessionStorage.getItem("idMembre"),
                  nom:this.state.nom,
                  prenom:this.state.prenom,
                  adresse:this.state.adresse,
                  filiere:this.state.filiere,
                  cin:this.state.cin,
                  delivreA:this.state.delivreA,
                  telephone:this.state.telephone,
                  domaine:this.state.domaine,
                  email:this.state.email,
                  dateNaissance:this.state.dateNaissance,
                  photo:this.state.photo
                }
               console.log(amodifierMembre)
                Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
                .then(response=>{
                   if(response){
                    window.alert('Modification terminée')
                    this.setState({
                        openModif:!this.state.openModif
                    }); 
                   }
                   })
                  }
                  else{
                    let res =  this.uploadFile(this.state.file1);
                    const amodifierMembre={
                      idMembre:sessionStorage.getItem("idMembre"),
                      nom:this.state.nom,
                        prenom:this.state.prenom,
                        adresse:this.state.adresse,
                        filiere:this.state.filiere,
                        cin:this.state.cin,
                        delivreA:this.state.delivreA,
                        telephone:this.state.telephone,
                        domaine:this.state.domaine,
                        email:this.state.email,
                        dateNaissance:this.state.dateNaissance,
                      photo:this.state.filename
                    }
                   console.log(amodifierMembre)
                    Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
                    .then(response=>{
                       if(response){
                        this.setState({
                            openModif:!this.state.openModif
                        }); 
                         window.alert('Modification terminée')
                        
                       }
                       })
                  }
        }
        UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
        onChangenom(e){
          this.setState({
            nom:e.target.value
          });
        }
        onChangeprenom(e){
          this.setState({
            prenom:e.target.value
          });
        }
        onChangeadresse(e){
          this.setState({
            adresse:e.target.value
          });
        }
        onChangefiliere(e){
          this.setState({
            filiere:e.target.value
          });
        }
        onChangecin(e){
          this.setState({
            cin:e.target.value
          });
        }
        onChangedelivreA(e){
          this.setState({
            delivreA:e.target.value
          });
        }
        onChangetelephone(e){
          this.setState({
            telephone:e.target.value
          });
        }
        onChangedomaine(e){
          this.setState({
            domaine:e.target.value
          });
        }
        onChangeemail(e){
          this.setState({
            email:e.target.value
          });
        }
        onChangedatenaissance(e){
          this.setState({
            dateNaissance:e.target.value
          });
        }
        onChangeFile(e) {
          this.setState({file:e.target.files[0]})
          this.setState({filename:e.target.files[0].name})
      }
      async uploadFile(file){
        if(file!=null){
          const data = new FormData();
          var originalfilename = file.name.split(".");
          data.append('file',file)
          data.append('upload_preset','insta-clone')
          data.append('cloud_name','ITUnivesity')
          data.append("public_id", originalfilename[0]);
      
          fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
              method:'post',
              body:data
          })
          .then(res=>res.json())
          .then(data=>{
            this.setState({imageurl:data.url})
            console.log(data)
          })
          .catch(err=>{
            console.log(err)
          })
        }
       else{}
      }
        modifier(){
            this.setState({
                openModif:!this.state.openModif
            });    
        }
        envoyer(e){
            e.preventDefault();
            const DemandeAccesMembre={
                idMembre:sessionStorage.getItem("idMembre"),
                estValide:'ko',
                motif:this.state.motif
              }
              console.log(DemandeAccesMembre)
            Axios.post('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/create.php',DemandeAccesMembre)
              .then(res=>{
                console.log(res.data)
                window.alert('Votre demande a été prise en compte.')
                this.setState({
                    openDemande:false,aitAcces:'pas'
                });
               this.componentDidMount();
              });

        }
        onChangeMotif(e){
            this.setState({
                motif:e.target.value
            });
          }
        onclick(){
            this.setState({openDemande:true,aitAcces:''})
        }
        annuler(){
            this.setState({openDemande:false,aitAcces:'pas'})
        }
        componentDidMount(){
            console.log("idMembre"+sessionStorage.getItem("idMembre"))
        //verifier si ait access
        Axios.get('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/single_read.php/?idMembre='+sessionStorage.getItem("idMembre"))
        .then(response=>{
            console.log("val="+response.data.estValide)
            this.setState({aitAcces:response.data.estValide})
        })
        .catch(error=>{
            this.setState({aitAcces:'pas'})
          
          })
        Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+sessionStorage.getItem("idMembre"))
        .then(response=>{
         
        this.setState({
            nom:response.data.nom,
            prenom:response.data.prenom,
            adresse:response.data.adresse,
            filiere:response.data.filiere,
            cin:response.data.cin,
            delivreA:response.data.delivreA,
            telephone:response.data.telephone,
            domaine:response.data.domaine,
            email:response.data.email,
            dateNaissance:response.data.dateNaissance,
            photo:response.data.photo,
        })
    
        })
    }  
render(){
    let aitacces;
    let inputDemande;
    let profil;
    if(this.state.openModif==true){

    }   profil=
        <p>
            <b>&nbsp;Nom:</b> <input onChange={this.onChangenom} style={{marginLeft:'35px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.nom}/><br/>
            <b>&nbsp;Prénom(s):</b><input onChange={this.onChangeprenom} style={{border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.prenom}/><br/>
            <b>&nbsp;Email:</b><input onChange={this.onChangeemail}  style={{marginLeft:'35px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.email}/><br/>
            <b>&nbsp;Adresse:</b> <input onChange={this.onChangeadresse} style={{marginLeft:'15px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.adresse}/><br/>
            <b>&nbsp;Filière:</b> <input onChange={this.onChangefiliere}  style={{marginLeft:'23px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.filiere}/><br/>
            <b>&nbsp;CIN:</b> <input  onChange={this.onChangecin} style={{marginLeft:'37px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.cin}/><br/>
            <b>&nbsp;Date de naissance:</b> <input onChange={this.onChangedatenaissance}  style={{border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.dateNaissance}/><br/>
            <b>&nbsp;Contact:</b><input  onChange={this.onChangetelephone} style={{marginLeft:'26px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.telephone}/><br/>
            <b>&nbsp;Domaine:</b><input onChange={this.onChangedomaine}  style={{marginLeft:'19px',border:'1px solid #d9d4d4',color:'gray',padding:'10px',height:'30px'}} type="text" value={this.state.domaine}/><br/>
            <div className="form-group">
                      <label for="exampleInputFile">Photo du membre</label>
                      <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>
                    </div>
            <button style={{padding: '10px 8px',border: 'none',backgroundColor:'gray',color:'white',width:'200px'}} onClick={this.enregistrer} >Enregister la modification</button>
        </p>
    if(this.state.openModif==false){
        profil=<p>
        <b>&nbsp;Nom:</b> {this.state.nom}<br/>
        <b>&nbsp;Prénom(s):</b>{this.state.prenom}<br/>
        <b>&nbsp;Email:</b> {this.state.email}<br/>
        <b>&nbsp;Adresse:</b> {this.state.adresse}<br/>
        <b>&nbsp;Filière:</b> {this.state.filiere}<br/>
        <b>&nbsp;CIN:</b> {this.state.cin}<br/>
        <b>&nbsp;Date de naissance:</b> {this.state.dateNaissance}<br/>
        <b>&nbsp;Contact:</b>{this.state.telephone}<br/>
         <b>&nbsp;Domaine:</b>{this.state.domaine}<br/>
       
       
     </p>
    }
    if(this.state.aitAcces=='pas'){
        aitacces=
        <div>
            <label>Demander accès pour modifier cette information</label><br/>
            <button style={{padding: '10px 8px',border: 'none',backgroundColor:'gray',color:'white',width:'200px'}} onClick={this.onclick} >Demander</button>
        </div>
    }
    if(this.state.aitAcces=='ok'){
        aitacces=<Link onClick={this.modifier} className="btn btn-primary">Modifier</Link>
    }
    if(this.state.aitAcces=='ko'){
        aitacces=
        <div>
            <label>Demander accès pour modifier cette information</label>
            <button style={{padding: '10px 8px',border: 'none',backgroundColor:'gray',color:'white',width:'200px'}} onClick={this.onclick} >Demander</button>
        </div>
    }
    if(this.state.aitAcces=='koko'){
        aitacces=
        <div>
            <label>Demander accès pour modifier cette information</label>
            <button style={{padding: '10px 8px',border: 'none',backgroundColor:'gray',color:'white',width:'200px'}} onClick={this.onclick} >Demander</button>
        </div>
    }
    if(this.state.openDemande==true){
      inputDemande=
      <form onSubmit={this.envoyer}>
          <input required type="text" style={{color:'gray',border:'1px solid #d1cfcf',padding:'10px'}} className="form-control" value={this.state.motif} placeholder="Motif de la demande" onChange={this.onChangeMotif} /><br/>
          <button type="submit" style={{padding: '10px 8px',border: 'none',backgroundColor:'gray',color:'white',width:'200px'}}>Envoyer ma demande</button>&nbsp;
          <button style={{padding: '10px 8px',border: 'none',backgroundColor:'#b83333',color:'white',width:'200px'}} onClick={this.annuler}>Annuler</button>
      </form>
    }
    if(this.state.openDemande==false){
        inputDemande=
        <></>
      }
    return(
         
        <section className="featured">
            
        <div className="container">
        <div className="row">
            
    
        <div className="col-md-6" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800"><br/><br/><br/></div>
        </div>
            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-6" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">
                    <div className="title">
                        <h6 className="title-primary"></h6>
                        <h1 className="title-blue">Votre profil</h1>
                    </div>
                    <table>
                        <tr>
                            <td>
                               {profil}
                             </td>
                            <td>
                           
                            <Avatar style={{width:200,height:200}} src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photo}`}/>
                  
                   
               
                            </td>
                        </tr>
                    </table>
                    
                    
                </div>
                <div className="col-md-3"></div>
               
            </div>

            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-6">
                   {inputDemande}
                   {aitacces}
                </div>
                <div className="col-md-3"></div>
            </div>

        </div>
    </section>
 
    )
}
}