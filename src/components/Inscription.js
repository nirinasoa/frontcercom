import React, { Component }  from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Link} from 'react-router-dom';
import Axios from 'axios';

export default class Inscription extends Component {
  constructor(props) {
    super(props)
    this.onChangeidMembre=this.onChangeidMembre.bind(this)
    this.onChangeemail=this.onChangeemail.bind(this)
    this.onChangePassword=this.onChangePassword.bind(this)
    this.onChangeconfirmPassword=this.onChangeconfirmPassword.bind(this)
    this.onSubmit=this.onSubmit.bind(this)

		this.state={
      email:'',
      password:'',
      confirmpassword:'',
      idmembre:'',
      errorpassword:'',
      errorchamp:'',
      errorNumero:'',
      errorweakpassword:''
     
      };
    
    }
    onChangeidMembre(e){
      this.setState({
        idmembre:e.target.value
      });
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
  
    onChangePassword(e){
      console.log("Nombre password"+e.target.value.split('').length)
      if(e.target.value.split('').length<4){
        this.setState({errorweakpassword:'Mot de passe trop court'})
      }
      else{
        this.setState({errorweakpassword:''})
      }
      this.setState({
        password:e.target.value
      });
    }
    onChangeconfirmPassword(e){
     
        this.setState({
          confirmpassword:e.target.value
        });
        if(this.state.password!== e.target.value){
          this.setState({
            errorpassword:'Mot de passe différent'
          });
        }
        else{
          this.setState({
            errorpassword:''
          });
        }
       
      }
    onSubmit(e){
      e.preventDefault();
    if(new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(this.state.email)==true){
      if(this.state.errorweakpassword!='Mot de passe trop court'){
      if(this.state.password!=this.state.confirmpassword){
        this.setState({
          errorchamp:'',
          errorpassword:'Mot de passe différent'
        });
      }
      else{
        this.setState({
          errorchamp:'',
          errorpassword:''
        });
        console.log("vrai")
        //Verifier si le numero membre existe dans base--Find where idMembre==this.state.idMembre
        Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+this.state.idmembre)
        .then(response=>{
         if(response.data.nom!=null){
            console.log('misy='+response.data.nom)
            //verif if the member id is already in table UtilisateurMembre  find idMembre=this.state.idMembre from it
            Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurMembre/single_read.php/?idMembre='+this.state.idmembre)
            .then(res=>{
              if(res.data.email!=null){
                console.log('idMembre existant!! vous ne pouvez pas inscrire='+res.data.email)
                this.setState({
                  errorNumero:'Ce numéro est déjà inscrit! Vous ne pouvez pas inscrire.',
               
                });
              }
              else{
                console.log('On peut créer votre compte')
              }
            })
            .catch(error=>{
              console.log(error+'=>On peut créer votre compte')
              Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+this.state.idmembre)
              .then(response=>{
                if(response.data.email!=this.state.email){
                  this.setState({errorNumero:''});
                 window.alert('Veuillez saisir votre adresse Email lors de l\'inscription à l\'administrateur!')
                }
                else{
                  console.log('On peut créer votre compte avec succès')
                  const membre={
                    idMembre:this.state.idmembre,
                    email:this.state.email,
                    motdepasse:this.state.password
                  }
                  console.log(membre)
                  Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurMembre/create.php',membre)
                  .then(res=>{
                    console.log(res.data);
                    window.alert('Votre compte a été crée avec succès!');
                    window.location='/';
                  });
                }
                
              })
             
            })
         }
         else{
          this.setState({
            errorNumero:'Numéro invalide! Veuillez contacter l\'administrateur CERCOM pour la création de votre numéro membre',
         
          });
         
         }
      
        })
     
       
      }
    
      }
      else{
        this.setState({errorweakpassword:'Mot de passe trop court'})
      }
    }
    else {
      this.setState({ errorchamp:'Email invalide'})
    }
   
      /*
      const membre={
        idMembre:this.state.idmembre,
        email:this.state.email,
        motdepasse:this.state.password
      }
      console.log(membre)
    Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurMembre/create.php',membre)
      .then(res=>{
        console.log(res.data)
       window.location='/';
      });
     */
     
    
    }
   render(){
  
      
    return (
       <div ><br/><br/><br/><h2 style={{"textAlign":"center","fontFamily":"Arial","color":"#4f4d4d"}}>
          <img   style={{width:'55px',borderRadius:'3px',height:"55px"}}  src="assets/images/iserazo.png" alt="CEROM" />&nbsp;
         <img src="assets/images/logo-cercom.jpg" width="60px"/>CERCOM|<span style={{color:'#575555',fontWeight:'normal'}}>Membre</span></h2><hr/>
       <Container component="main" maxWidth="xs" style={{ "backgroundColor": "#f2f0f0","borderRadius":"10px","border":"1px solid #c9c7c7","width":"500px","alignItems":"center","justifyContent":"center"}}>
    
    
    <CssBaseline />
    <br/>
    <br/>
    <div >
      
      
      <Typography component="h1" variant="h5" style={{"textAlign":"center"}}>
      <Avatar  style={{"marginLeft":"180px","backgroundColor":"#4a3bed"}}>
        <LockOutlinedIcon style={{"color":""}} />
      </Avatar>
        Inscription
      </Typography>
    
      <label style={{"marginLeft":"90px","fontFamily":"Arial"}}>Veuillez remplir les champs suivants</label>

      <br/> 
      
      <b style={{"marginLeft":"20px",color:'#cf4040',"fontFamily":"Arial"}}>{this.state.errorNumero}</b>
      <br/><br/>
      <form onSubmit={this.onSubmit}>
        <Grid container spacing={2}>
        <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="idmembre"
              label="Numéro d'identification"
              type="text"
              value={this.state.idmembre}
              onChange={this.onChangeidMembre}
            />
          </Grid>
          <Grid item xs={12}>
          <label style={{color:'#cf4040',"fontFamily":"Arial"}}>{this.state.errorchamp}</label>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="email"
              label="Votre adresse email"
              type="email"
              value={this.state.email}
              onChange={this.onChangeemail}
              
            />
          </Grid>
        
          <Grid item xs={12}>
          <label style={{color:'#cf4040',"fontFamily":"Arial"}}>{this.state.errorweakpassword}</label>
            <TextField
              maxLength={5}
             variant="outlined"
              required
              fullWidth
              name="password"
              label="Mot de passe"
              type="password"
              id="password"
              autoComplete="current-password"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
          </Grid>
          <Grid item xs={12}>
          <label style={{color:'#cf4040'}}>{this.state.errorpassword}</label>
            <TextField
              variant="outlined"
              required
              fullWidth
              name="password"
              label="Confirmation de mot de passe"
              type="password"
              id="confirm"
              autoComplete="current-password"
              value={this.state.confirmpassword}
              onChange={this.onChangeconfirmPassword}
            />
          </Grid>
          
        </Grid>
        <br/>
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
          
          >
           S'inscrire
          </Button><br/><br/>
          <Grid container>
            <Grid item xs>
              <Link  style={{"color":"#4a3bed"}} to="/" variant="body2">
               Revenir vers la page d'accueil
              </Link>
            </Grid>
            <Grid item>
              <Link style={{"color":"#4a3bed"}} to="/login" variant="body2">
                {"Login"}
              </Link>
            </Grid>
          </Grid>
        
       
      </form>
      <br/>
    </div>
  
  </Container>

       </div>
    )
   }
}