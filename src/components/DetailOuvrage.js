import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Avatar from '@material-ui/core/Avatar';
import Moment from 'moment';
import 'moment/locale/fr';
import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import FastfoodIcon from '@material-ui/icons/Fastfood';
import LaptopMacIcon from '@material-ui/icons/LaptopMac';
import HotelIcon from '@material-ui/icons/Hotel';
import RepeatIcon from '@material-ui/icons/Repeat';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import LocalHotelIcon from '@material-ui/icons/LocalHotel';
import MicIcon from '@material-ui/icons/Mic';
import IconButton from "@material-ui/core/IconButton";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';


const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const recognition = new SpeechRecognition()

recognition.continous = true
recognition.interimResults = true


export default class DetailOuvrage extends Component {
    constructor(props) {
        super(props)
        this.onChangemessage=this.onChangemessage.bind(this)
        this.posterCommentaire=this.posterCommentaire.bind(this)
        this.toggleListen = this.toggleListen.bind(this)
        this.handleListen = this.handleListen.bind(this)
        
         this.state={
           idOuvrage:this.props.match.params.id,
           ouvrages:[],
           titre:'',
           editeur:'',
           nombrePage:1,
           prix:1,
           nomGenre:'',
           langue:'',
           anneeEdition:'',
           quantite:1,
           origine:'',
           lieuEdition:'',
           descriptionGenre:'',
           description:'',
           nomAuteur:'',
           dateEntree:'',
          photo:'',
          etatActuel:'',
          created:'',
          genres:[],
          nom:'',
          prenom:'',
          adresse:'',
          filiere:'',
          cin:'',
          delivreA:'',
          telephone:'',
          domaine:'',
          email:'',
          dateNaissance:'',
          photoM:'',
          message:'',
          commentaires:[],
          nombre:'',
          listening: false,
          finalTranscript:'',
          bgColor: '#4b63db',
          lang:'fr-FR',
          anchorEl:null
       
        
        };
      }
       handleClick = (event) => {
        this.setState({anchorEl:event.currentTarget});
      };
    
       handleClose = () => {
        this.setState({anchorEl:null});
      };
      toggleListen(event) {
   
        this.setState({
      
          bgColor: '#e82a33',
          listening: !this.state.listening
        }, this.handleListen)
      }
      
      handleListen(){
        
        recognition.lang = this.state.lang
      
            if (this.state.listening) {
                recognition.start()
          
              let finalTranscript = ''
              recognition.onresult = event => {
                let interimTranscript = ''
          
                for (let i = event.resultIndex; i < event.results.length; i++) {
                  const transcript = event.results[i][0].transcript;
                  if (event.results[i].isFinal) finalTranscript += transcript + ' ';
                  else interimTranscript += transcript;
                }
                this.setState({message:finalTranscript});
              if(this.state.message!=''){
                this.setState({bgColor:'#4b63db'});
                
              }
            }
          }
          else{
            recognition.stop()
           
           
          }
        
       
      
    }
      posterCommentaire(){
        const commentaire={
            idMembre:sessionStorage.getItem("idMembre"),
            idOuvrage:this.props.match.params.id,
            message:this.state.message
          }
          console.log(commentaire)
          if(this.state.message!=''){
            Axios.post('https://phpapiserver.herokuapp.com/api/commentaire/create.php',commentaire)
            .then(res=>{
              console.log(res.data)
              this.setState({
                  message:''
              });
            
             this.componentDidMount();
            });
          }
          else{
              alert('Veuillez poster un commentaire avant de valider!')
          }
     
      }
      onChangemessage(e){
        this.setState({
            message:e.target.value
        });
      }
      componentDidMount(){
        Axios.get('https://phpapiserver.herokuapp.com/api/commentaire/nombreCommentaireParOuvrage.php/?idOuvrage='+this.props.match.params.id)
        .then(response=>{
            if(response.data.nombre!=null){
                console.log('nombre: '+response.data.nombre+' commentaire(s)')
                 this.setState({nombre:response.data.nombre+' commentaire(s)'})
            }
            else{
                console.log('Aucun:')
                this.setState({nombre:'Aucun commentaire'})
            }
        })
         
        console.log("idMembre"+sessionStorage.getItem("idMembre"))
        Axios.get('https://phpapiserver.herokuapp.com/api/commentaire/ficheCommentaireOk.php/?idOuvrage='+this.props.match.params.id)
        .then(response=>{
            if(response.data.length>0){
                console.log("ato="+response.data)
                this.setState({commentaires:response.data})
            }
            else{
                this.setState({nombre:'Aucun commentaire',commentaires:[]})
            }
          
        })
        .catch(error=>{
            console.log(error)
          
          })
        Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+sessionStorage.getItem("idMembre"))
        .then(response=>{
         
        this.setState({
            nom:response.data.nom,
            prenom:response.data.prenom,
            adresse:response.data.adresse,
            filiere:response.data.filiere,
            cin:response.data.cin,
            delivreA:response.data.delivreA,
            telephone:response.data.telephone,
            domaine:response.data.domaine,
            email:response.data.email,
            dateNaissance:response.data.dateNaissance,
            photoM:response.data.photo,
        })
    
        })
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClmView.php/?idOuvrage='+this.props.match.params.id)
        .then(response=>{
        this.setState({
          nomGenre:response.data.nomGenre,
          descriptionGenre:response.data.descriptionGenre,
          nomAuteur:response.data.nomAuteur,
          titre:response.data.titre,
          editeur:response.data.editeur,
          nombrePage:response.data.nombrePage,
          prix:response.data.prix,
          anneeEdition:response.data.anneeEdition,
          dateEntree:response.data.dateEntree,
          quantite:response.data.quantite,
          quantite:response.data.quantite,
          origine:response.data.origine,
          lieuEdition:response.data.lieuEdition,
          description:response.data.description,
          photo:response.data.photo,
          created:response.data.created,
          etatActuel:response.data.etatActuel,

    
        })})
        .catch(error=>{
          console.log(error)
        })
       };
    render(){
        let buttonposterComentaire;
        let posterComms;
        if(sessionStorage.getItem("idMembre")==null){
            posterComms=<></>
        }
        if(sessionStorage.getItem("idMembre")!=null){
            posterComms=<div className="col-md-12" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">
          
            <div style={{display:'inline-block'}}>
              
                <h5>  <i className="fa fa-envelope mr-4"></i>&nbsp;Poster un commentaire 
                
                                <IconButton onClick={this.toggleListen}>
                                      <MicIcon style={{"color":this.state.bgColor}}/>
                                </IconButton>
                               
                </h5>
            </div>
            <div>
                <table>
                    <tr>
                        <td> <Avatar  style={{width:35,height:35}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`}  /></td>
                        <td> <TextareaAutosize  aria-label="minimum height" rowsMin={1}  style={{border:'1px solid #cccaca',padding:'10px',borderRadius:'5px',color:'gray',marginLeft:'5px',marginRight:'5px',display: 'block',width: '225%',overflow: 'hidden',resize: 'both',minHeight: '60px',lineHeight: '20px'}}  type="text" required className="form-control" id="commentaire" placeholder="Rejoindre la discussion..." value={this.state.message} onPaste={this.onChangemessage} onChange={this.onChangemessage}/></td>
                    </tr>
                    <tr>
                       <td>
                              
                       </td>
                       <td style={{}}><br/> <button onClick={this.posterCommentaire} style={{padding: '10px 8px',border: 'none',marginLeft:'128%',backgroundColor:'gray',color:'white',width:'200px'}}>Poster le commentaire</button></td>
                    </tr>       
                </table>
            </div>
           
        </div>
        }
     
    return(
         
    <section className="featured">
        <div className="container">
            <div className="row">
                <div className="col-md-12" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800"><br/><br/><br/></div>
            </div>
            <div className="row">
                <div className="col-md-6" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">
                    <div className="title">
                        <h6 className="title-primary"></h6>
                        <h1 className="title-blue">{this.state.titre}</h1>
                    </div>
                    <p>
                                <b>&nbsp;Auteur:</b> {this.state.nomAuteur}<br/>
                                <b>&nbsp;Titre:</b> {this.state.titre}<br/>
                                <b>&nbsp;Editeur:</b> {this.state.editeur}<br/>
                                <b>&nbsp;Prix:</b> {this.state.prix}<br/>
                                <b>&nbsp;Genre:</b> {this.state.nomGenre}<br/>
                                <b>&nbsp;Langue:</b>{this.state.langue}<br/>
                                <b>&nbsp;Année d'édition:</b> {this.state.anneeEdition}<br/>
                                <b>&nbsp;Date d'entrée:</b> {this.state.dateEntree}<br/>
                                <b>&nbsp;Quantité en stock:</b> {this.state.quantite}<br/>
                                <b>&nbsp;Origine:</b> {this.state.origine}<br/>
                                <b>&nbsp;Lieu d'édition:</b>{this.state.lieuEdition}<br/>
                                <b>&nbsp;Description:</b> {this.state.description}
                        
                    </p>
                    
                </div>
                <div className="col-md-6" data-aos="fade-left" data-aos-delay="400" data-aos-duration="800">
                     <br/> <br/>
                    <Avatar   src={decodeURIComponent( decodeURIComponent(this.state.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}  style={{"borderRadius":"5px",width:"320px",height:"420px"}}alt="Featured 1"/>
                
                </div>
               
            </div>
            
            <div className="row">
               {posterComms}
               
            </div>
            <div className="row">
                <div className="col-md-6" data-aos="fade-right" data-aos-delay="200" data-aos-duration="400">
                     <div>
                     {this.state.nombre} 
                     { this.state.commentaires.map(comms=>
                       <div>
                        <table style={{backgroundColor:'white',borderRadius:'5px',border:'1px solid #f0eded'}}>
                            
                            <tr>
                                <td>&nbsp; <Avatar  style={{width:35,height:35}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${comms.photo}`}  /></td>
                                <td style={{width:'92%'}}>&nbsp;<b style={{fontFamily:'Arial',fontSize:'14px'}}>{comms.nom}&nbsp;{comms.prenom}</b><br/>&nbsp;<span style={{fontFamily:'Arial',fontSize:'14px',color:'#949191'}}>{comms.message}</span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style={{width:'92%'}}><span style={{marginLeft:'65%',color:'gray',fontFamily:'Arial',fontSize:'13px'}}>{Moment(comms.created).locale("de").format('Do MMMM YYYY')}  {Moment(comms.created).locale("de").format('HH:mm')}</span></td>
                            </tr>
                            
                            </table> 
                            <br/>   
                       </div>
                     )}
                     
                    
                    </div>
                </div>
                <div className="col-md-6" data-aos="fade-right" data-aos-delay="200" data-aos-duration="400">
                <Timeline align="alternate">
                    <TimelineItem>
                        <TimelineOppositeContent>
                        <Typography variant="body2" color="textSecondary">
                            9:30 am
                        </Typography>
                        </TimelineOppositeContent>
                        <TimelineSeparator>
                        <TimelineDot>
                            <FastfoodIcon />
                        </TimelineDot>
                        <TimelineConnector />
                        </TimelineSeparator>
                        <TimelineContent>
                        <Paper elevation={2} style={{ padding: '6px 6px'}}>
                            <Typography variant="h6" component="h1">
                            Manger
                            </Typography>
                            <Typography>Pour être productif</Typography>
                        </Paper>
                        </TimelineContent>
                    </TimelineItem>
                    <TimelineItem>
                        <TimelineOppositeContent>
                        <Typography variant="body2" color="textSecondary">
                            10:00 am
                        </Typography>
                        </TimelineOppositeContent>
                        <TimelineSeparator>
                        <TimelineDot color="primary">
                            <MenuBookIcon />
                        </TimelineDot>
                        <TimelineConnector />
                        </TimelineSeparator>
                        <TimelineContent>
                        <Paper elevation={3} style={{ padding: '6px 10px'}}>
                            <Typography variant="h6" component="h1">
                            Lire
                            </Typography>
                            <Typography>Pour acquérir votre connaissance!</Typography>
                        </Paper>
                        </TimelineContent>
                    </TimelineItem>
                    <TimelineItem>
                        <TimelineSeparator>
                        <TimelineDot color="primary" >
                            <LocalHotelIcon />
                        </TimelineDot>
                        <TimelineConnector />
                        </TimelineSeparator>
                        <TimelineContent>
                        <Paper elevation={3} style={{ padding: '6px 16px'}} >
                            <Typography variant="h6" component="h1">
                            Dormir
                            </Typography>
                            <Typography>Vous avez besoin d'un repos</Typography>
                        </Paper>
                        </TimelineContent>
                    </TimelineItem>
                    <TimelineItem>
                        <TimelineSeparator>
                        <TimelineDot color="secondary">
                            <RepeatIcon />
                        </TimelineDot>
                        </TimelineSeparator>
                        <TimelineContent>
                        <Paper elevation={3} style={{ padding: '6px 16px'}}>
                            <Typography variant="h6" component="h1">
                            Répéter
                            </Typography>
                            <Typography>C'est la vie de vos rêves!</Typography>
                        </Paper>
                        </TimelineContent>
                    </TimelineItem>
                    </Timeline>
                </div>
            </div>
            <Link to="/accueil" className="btn btn-primary">Revenir vers la liste</Link>
               
           

        </div>
    </section>
 
    )
}
}