import React, { Component }  from 'react';
import { Link } from 'react-router-dom';

export default class Banner extends Component {
   render(){
    return (
       
        <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-12 offset-md-1 col-md-11">
                    <div class="swiper-container hero-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide slide-content d-flex align-items-center">
                                <div class="single-slide">
                                    <h1 data-aos="fade-right" data-aos-delay="200">Université d'Antananarivo<br/> Bienvenue dans notre site
                                    </h1>
                                    <p data-aos="fade-right" data-aos-delay="600">Ce site est destiné aux lecteurs membres.
                                        Dans ce système, 
                                        il existe<br/> quelques fonctionnalité que vous pouvez exécuter.
                                    </p>
                                    <a data-aos="fade-right" data-aos-delay="900" href="mailto:info@univ-antananarivo.mg" class="btn btn-primary">Contacter</a>
                                    <Link to="/login" data-aos="fade-right" data-aos-delay="900" href="#" class="btn btn-primary">Connexion</Link>
                                </div>
                            </div>
                            <div class="swiper-slide slide-content d-flex align-items-center">
                                <div class="single-slide">
                                    <h1 data-aos="fade-right" data-aos-delay="200">CERCOM<br/>Domaine des Arts, Lettres et Sciences Humaines
                                    </h1>
                                    <p data-aos="fade-right" data-aos-delay="600">Ce site est destiné aux lecteurs membres.
                                        Dans ce système, 
                                        il existe<br/> quelques fonctionnalité que vous pouvez exécuter.
                                    </p>
                                    <a data-aos="fade-right" data-aos-delay="900" href="mailto:info@univ-antananarivo.mg" class="btn btn-primary">Contacter</a>
                                    <Link to="/login" data-aos="fade-right" data-aos-delay="900" href="#" class="btn btn-primary">Connexion</Link>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
           
            <span class="arr-left"><i class="fa fa-angle-left"></i></span>
            <span class="arr-right"><i class="fa fa-angle-right"></i></span>
        </div>
        <div class="texture"></div>
        <div class="diag-bg"></div>
    </section>
    )
   }
}