import React, { Component }  from 'react';
import Banner from './Banner';
import BasBanner from './BasBanner';
import Ouvrage from './Ouvrage';
import ProgressionLecture from './ProgressionLecture';

export default class Accueil extends Component {
render(){
    let progression;
    if(sessionStorage.getItem("idMembre")!=null){
        progression=<ProgressionLecture/>
    }
    if(sessionStorage.getItem("idMembre")==null){
        progression=  <></>
    }
    
    return(
        <div>
            <Banner/>
            <BasBanner/>
            <Ouvrage/>
            {progression}
        </div>
    )
}
}