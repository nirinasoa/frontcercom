import React, { Component }  from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Link} from 'react-router-dom';
import axios from 'axios';

export default class Login extends Component {
  constructor(props) {
    super(props)
    
    this.onChangeemail=this.onChangeemail.bind(this)
    this.onChangePassword=this.onChangePassword.bind(this)
    this.onSubmit=this.onSubmit.bind(this)

		this.state={
      email:'',
      password:'',
      erreur:'',
      
      };
    
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
  
    onChangePassword(e){
      this.setState({
        password:e.target.value
      });
    }
    onSubmit(e){
      e.preventDefault();
      const data={
        email:this.state.email,
        motdepasse:this.state.password
      }
      axios.post('https://phpapiserver.herokuapp.com/api/utilisateurMembre/login.php',data)
      .then(res=> {
        if(res.data.message=="Successful login."){
          sessionStorage.setItem("loggedIn","true");
          sessionStorage.setItem("jwt",res.data.jwt);
         sessionStorage.setItem("idMembre",res.data.idMembre);
         window.location="/"
        }
        else{
          this.setState({erreur:'Erreur de mot de passe ou identifiant invalide'})
        }
       
      
      })
      .catch(error => {
        this.setState({erreur:'Erreur de mot de passe ou identifiant invalide'})
       })
  }

   render(){
  
      
    return (
       <div ><br/><br/><br/><h2 style={{"textAlign":"center","fontFamily":"Arial","color":"#4f4d4d"}}>
         <img   style={{width:'55px',borderRadius:'3px',height:"55px"}}  src="assets/images/iserazo.png" alt="CEROM" />&nbsp;
         <img src="assets/images/logo-cercom.jpg" width="60px" height="60px"/>CERCOM|<span style={{color:'#575555',fontWeight:'normal'}}>Membre</span></h2><hr/>
       <Container component="main" maxWidth="xs" style={{ "borderRadius":"10px","border":"1px solid #c9c7c7","width":"500px","alignItems":"center","justifyContent":"center"}}>
    
    
    <CssBaseline />
    <br/>
    <br/>
    <div >
      
      
      <Typography component="h1" variant="h5" style={{"textAlign":"center"}}>
      <Avatar  style={{"marginLeft":"180px","backgroundColor":"#4a3bed"}}>
        <LockOutlinedIcon style={{"color":""}} />
      </Avatar>
        Login
      </Typography>
      <label style={{"marginLeft":"100px","fontFamily":"Arial"}}>Veuillez remplir les champs suivants</label>
      <br/>  <label style={{"fontWeight":"bold","fontFamily":"Arial","fontSize":"13px","color":"#ba2f2f"}}>{this.state.erreur} </label>
      <form onSubmit={this.onSubmit}>
        <Grid container spacing={2}>
        
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="email"
              type="email"
              label="Numéro d'identification"
              name="email"
              value={this.state.email}
              onChange={this.onChangeemail}
            />
          </Grid>
        
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              name="password"
              label="Mot de passe"
              type="password"
              id="password"
              autoComplete="current-password"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
          </Grid>
          
        </Grid>
        <br/>
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
          
          >
           Se connecter
          </Button><br/>
          <br/>
          <Grid container>
            <Grid item xs>
              <Link  style={{"color":"#4a3bed"}} to="/" variant="body2">
               Revenir vers la page d'accueil
              </Link>
            </Grid>
            <Grid item>
              <Link style={{"color":"#4a3bed"}} to="/inscription" variant="body2">
                {"Pas de compte?"}
              </Link>
            </Grid>
          </Grid>
       
      </form>
      <br/>
    </div>
  
  </Container>

       </div>
    )
   }
}