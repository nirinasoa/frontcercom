import React, { Component }  from 'react';
import {  Switch, Route } from 'react-router-dom'
import Content from '../Content'
import Login from '../LoginPage/Login'
import Inscription from '../Inscription';
import Ouvrage from '../Ouvrage';
import Accueil from '../Accueil';

export default class Routes extends Component {
    render() {
   
        return (
           
            <Switch>
                    <Route path='/' component={Content} exact/>
                    <Route path='/inscription' component={Inscription} exact/>
                    <Route path='/login' component={Login} exact/>
                    <Route path='/accueil' component={Content} exact/>
                    <Route path='/detailOuvrage/:id' component={Content} exact/>
                    <Route path='/ouvrage' component={Content} exact/>
                    <Route path='/profil' component={Content} exact/>
                    <Route path='/autre' component={Content} exact/>
                    <Route path='/favori' component={Content} exact/>
                    <Route path='/progressionLecture' component={Content} exact/>
                    <Route path='/commentaireOuvrage/:id' component={Content} exact/>
            </Switch>
            
           
        
        )
    }
}
