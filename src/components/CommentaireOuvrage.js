import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';

export default class CommentaireOuvrage extends Component {
    constructor(props) {
        super(props)
         this.state={
           idOuvrage:this.props.match.params.id,
           ouvrages:[],
           titre:'',
           editeur:'',
           nombrePage:1,
           prix:1,
           nomGenre:'',
           langue:'',
           anneeEdition:'',
           quantite:1,
           origine:'',
           lieuEdition:'',
           descriptionGenre:'',
           description:'',
           nomAuteur:'',
           dateEntree:'',
          photo:'',
          etatActuel:'',
          created:'',
          genres:[],
        
        };
      }
      componentDidMount(){
      
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClmView.php/?idOuvrage='+this.props.match.params.id)
        .then(response=>{
        this.setState({
          nomGenre:response.data.nomGenre,
          descriptionGenre:response.data.descriptionGenre,
          nomAuteur:response.data.nomAuteur,
          titre:response.data.titre,
          editeur:response.data.editeur,
          nombrePage:response.data.nombrePage,
          prix:response.data.prix,
          anneeEdition:response.data.anneeEdition,
          dateEntree:response.data.dateEntree,
          quantite:response.data.quantite,
          quantite:response.data.quantite,
          origine:response.data.origine,
          lieuEdition:response.data.lieuEdition,
          description:response.data.description,
          photo:response.data.photo,
          created:response.data.created,
          etatActuel:response.data.etatActuel,

    
        })})
        .catch(error=>{
          console.log(error)
        })
       };
    render(){
    return(
         
        <section className="featured">
            
        <div className="container">
        <div className="row">
        <div className="col-md-12" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800"><br/><br/><br/></div>
        </div>
            <div className="row">
                <div className="col-md-6" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">
                    <div className="title">
                        <h6 className="title-primary"></h6>
                        <h1 className="title-blue">{this.state.titre}</h1>
                    </div>
                    <p>
                                <b>&nbsp;Auteur:</b> {this.state.nomAuteur}<br/>
                                <b>&nbsp;Titre:</b> {this.state.titre}<br/>
                                <b>&nbsp;Editeur:</b> {this.state.editeur}<br/>
                                <b>&nbsp;Prix:</b> {this.state.prix}<br/>
                                <b>&nbsp;Genre:</b> {this.state.nomGenre}<br/>
                                <b>&nbsp;Langue:</b>{this.state.langue}<br/>
                                <b>&nbsp;Année d'édition:</b> {this.state.anneeEdition}<br/>
                                <b>&nbsp;Date d'entrée:</b> {this.state.dateEntree}<br/>
                                <b>&nbsp;Quantité en stock:</b> {this.state.quantite}<br/>
                                <b>&nbsp;Origine:</b> {this.state.origine}<br/>
                                <b>&nbsp;Lieu d'édition:</b>{this.state.lieuEdition}<br/>
                                <b>&nbsp;Description:</b> {this.state.description}
                        
                    </p>
                    
                </div>
                <div className="col-md-6" data-aos="fade-left" data-aos-delay="400" data-aos-duration="800">
                    <div className="featured-img">
                        <img className="featured-big"  src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photo}`} width="320px" style={{"borderRadius":"5px"}}alt="Featured 1"/>
                    </div>
                </div>
               
            </div>
            <div className="row">
                <div className="col-md-12">
                <div className="media-element d-flex justify-content-between">
                        <div className="media">
                            <i className="fa fa-magic mr-4"></i>
                            <div className="media-body">
                                <h5>Progression de lecture</h5>
                                10%
                            </div>
                        </div>
                      
                    </div>
                    <Link to="/accueil" className="btn btn-primary">Revenir vers la liste</Link>
                </div>
            </div>

        </div>
    </section>
 
    )
}
}