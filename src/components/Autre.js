import React, { Component }  from 'react';
import { Link } from 'react-router-dom';


export default class Autre extends Component {
render(){
    return(
         
        <section className="featured">
            
        <div className="container">
        <div className="row">
        <div className="col-md-12" data-aos="fade-right" data-aos-delay="300" data-aos-duration="700"><br/><br/><br/></div>
        </div>
            <div className="row">
                <div className="col-md-12" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">
                    <div className="title">
                      
                        <h1 className="title-blue">INFORMATIONS</h1>
                    </div>
                   <p>
                       <h4>La partie Membre</h4><hr/>
                   <h5>1. S’identifier</h5>
                    Le rôle du membre est tout d’abord récupérer son carte membre au moniteur pour la création de son propre profil en saisissant juste MEMBER_ID, son nouveau mot de passe et la confirmation de son nouveau mot de passe.
                     

                    <h5>2. Accès membre</h5>
                    Le membre permet d’effectuer une recherche souple selon un critère défini. Il peut consulter des ouvrages, de voir son propre profil, de voir les livres les plus lus.
                    <h5>3. Gestion du profil</h5>
                    Chaque membre pourra gérer son profil, c’est-à-dire il pourra changer son profil si la demande a été validée par le personnel .
                                                
                             </p>
                           
                    
                    
                </div>
              
               
            </div>
            <div className="row">
                <div className="col-md-12">
                
                    <Link to="/accueil" className="btn btn-primary">Revenir vers accueil</Link>
                </div>
            </div>

        </div>
    </section>
 
    )
}
}