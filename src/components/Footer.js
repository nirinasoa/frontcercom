import React, { Component }  from 'react';
import Axios from 'axios';

export default class Footer extends Component {
    constructor(props) {
      super(props)
      this.onSubmit=this.onSubmit.bind(this);
		this.state={
				   
			pluslu:[],
				   
		  };
		
		}
    componentDidMount(){
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/pluslu.php')
		.then(response=>{
		  this.setState({pluslu:response.data})
		  
		})
    }
    onSubmit(){
        window.alert('Merci pour votre participation!')
    }
 
render(){
    return(
        <footer>
       <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xl-3">
                        <div class="single-widget contact-widget" data-aos="fade-up" data-aos-delay="0">
                            <h6 class="widget-tiltle">&nbsp;</h6>
                           <p>Une solution web pour la gestion des ouvrages</p>
                            <div class="media">
                                <i class="fa fa-map-marker"></i>
                                <div class="media-body ml-3">
                                    <h6>Université Ankatso</h6>
                                   
                                    Facultés des sciences
                                </div>
                            </div>
                            <div class="media">
                                <i class="fa fa-envelope-o"></i>
                                <div class="media-body ml-3">
                                    <h6>Nous contacter?</h6>
                                    <a href="mailto:info@univ-antananarivo.mg">info@univ-antananarivo.mg</a>
                                </div>
                            </div>
                            <div class="media">
                                <i class="fa fa-phone"></i>
                                <div class="media-body ml-3">
                                    <h6>Numéro</h6>
                                    <a href="tel:+261 20 22 326 39"> +261 20 22 326 39</a>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="single-widget twitter-widget" data-aos="fade-up" data-aos-delay="200">
                            <h6 class="widget-tiltle">CERCOM</h6>
                            
                           
                            <div class="media">
                                <i class="fa fa-twitter"></i>
                                <div class="media-body ml-3">
                                    <h6>Gestion des ouvrages</h6>
                                    <span>Moniteurs et Membres</span>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                    <div class="col-md-6 col-xl-3">
                        <div class="single-widget recent-post-widget" data-aos="fade-up" data-aos-delay="400">
                            <h6 class="widget-tiltle">Les dernières mis à jour</h6>
                            { this.state.pluslu.map(pl=>   
                            <div class="media">
                                <a class="rcnt-img" href="#"><img  src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${pl.photo}`}/></a>
                                    
                                <div class="media-body ml-3">
                                    <h6><a href="#">{pl.titre}</a></h6>
                                    <p><i class="fa fa-user"></i>{pl.nomAuteur} <i class="fa fa-eye"></i> {pl.max}</p>
                                </div>
                                
                            </div>
                              )}
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="single-widget tags-widget" data-aos="fade-up" data-aos-delay="800">
                            <h6 class="widget-tiltle">Ratez notre site</h6>
                          
                        </div>
                        <div class="single-widget subscribe-widget" data-aos="fade-up" data-aos-delay="800">
                            <h6 class="widget-tiltle"></h6>
                            <p></p>
                           
                                <div class="input-group">
                                    <input class="field form-control" name="subscribe" type="text"
                                        placeholder="Ce site est utile pour vous?"/>
                                    <span class="input-group-btn">
                                        <button onClick={this.onSubmit} name="submit-mail"><i class="fa fa-check"></i></button>
                                    </span>
                                </div>
                          
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="foot-note">
            <div class="container">
                <div
                    class="footer-content text-center text-lg-left d-lg-flex justify-content-between align-items-center">
                    <p class="mb-0" style={{"color":"white"}} data-aos="fade-right" data-aos-offset="0">&copy; 2020 All Rights Reserved by RYN.</p>
                   
                </div>
            </div>
        </div>
   </footer>
    )
}
}