import React, { Component }  from 'react';
import {  Switch, Route,Redirect} from 'react-router-dom'
import Header from './Header';
import Accueil from './Accueil';
import Footer from './Footer';
import DetailOuvrage from './DetailOuvrage';
import Ouvrage from './Ouvrage';
import Profil from './Profil';
import Autre from './Autre';
import ProgressionLecture from './ProgressionLecture';
import CommentaireOuvrage from './CommentaireOuvrage';
import Favori from './Favori';

export default class Content extends Component {
    constructor(props){
        super(props);
    }  
       
    render() {
    
    return (
        <div>
        
            <Header/>
           
            <Switch>
                 <Route  path="/" component={Accueil} exact/>
                 <Route  path="/detailOuvrage/:id" component={DetailOuvrage} exact/>
                 <Route  path="/ouvrage" component={Ouvrage} exact/>
                 <Route  path="/profil" component={Profil} exact/>
                 <Route  path="/autre" component={Autre} exact/>
                 <Route  path="/progressionLecture" component={ProgressionLecture} exact/>
                 <Route  path="/favori" component={Favori} exact/>
                 <Route  path="/commentaireOuvrage/:id" component={CommentaireOuvrage} exact/>
               
            </Switch>
            <Footer/>
        </div>
      );
    }
    }