import React from 'react';
import {BrowserRouter} from 'react-router-dom'
import './App.css';
import Routes from './components/Route/Routes';


function App() {
  return (
   <div>
      <div class="css-loader">
        <div class="loader-inner line-scale d-flex align-items-center justify-content-center"></div>
    </div>
  
    <BrowserRouter >
       <Routes/> 
      
    </BrowserRouter>
    </div>
  );
}

export default App;