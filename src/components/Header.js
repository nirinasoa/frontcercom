import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import Moment from 'moment';
import 'moment/locale/fr';
import FavoriteIcon from '@material-ui/icons/Favorite';

export default class Header extends Component {
    constructor(props) {
        super(props)
        this.state={
            idmembre:sessionStorage.getItem("idMembre"),
            nom:'',
	        prenom:'',
            adresse:'',
            filiere:'',
            cin:'',
            delivreA:'',
            telephone:'',
            domaine:'',
            email:'',
            dateNaissance:'',
            photo:'',
            anchorEl:null,
            resultatValidation:'',
            datedemande:'',
            notif:0,
            motif:''
         
          };
        
        }
        handleClose = () => {
            this.setState({anchorEl:null})
          };
        handleClick = (event) => {
            this.setState({anchorEl:event.currentTarget,notif:0})
          };
        componentDidMount(){
        
            console.log("idMembre"+sessionStorage.getItem("idMembre"))
        Axios.get('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/single_read.php/?idMembre='+sessionStorage.getItem("idMembre"))
            .then(response=>{
                if(response.data.estValide=='ko'){
                    this.setState({motif:response.data.motif,resultatValidation:'Votre demande est en attente',notif:0,datedemande:response.data.created})
                }
                else if(response.data.estValide=='ok'){
                    this.setState({motif:response.data.motif,resultatValidation:'Votre demande a été validée',notif:1,datedemande:response.data.created})
                }
                else if(response.data.estValide=='koko'){
                    this.setState({motif:response.data.motif,resultatValidation:'Votre demande a été réfusée',notif:1,datedemande:response.data.created})
                }
                else{
                    this.setState({datedemande:response.data.created})
                }
               
            })
            .catch(error=>{
                this.setState({notif:0,resultatValidation:'Aucune notification'})
              
              })
        Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+sessionStorage.getItem("idMembre"))
        .then(response=>{
         
        this.setState({
            nom:response.data.nom,
            prenom:response.data.prenom,
            adresse:response.data.adresse,
            filiere:response.data.filiere,
            cin:response.data.cin,
            delivreA:response.data.delivreA,
            telephone:response.data.telephone,
            domaine:response.data.domaine,
            email:response.data.email,
            dateNaissance:response.data.dateNaissance,
            photo:response.data.photo,
        })
    
        })
    }  
    deconnexion = () => {
        window.sessionStorage.clear()
        window.location="/"
       
      };
   render(){
       let result;
       let nav;
       let estConnete;
       let profil;
       let progressionL;
       if(sessionStorage.getItem("idMembre")!=null){
        progressionL=   <li className="nav-item"><Link className="nav-link" to="/progressionLecture">Progression lecture</Link></li>
    }
    if(sessionStorage.getItem("idMembre")==null){
        progressionL= <></>
    }
       if(sessionStorage.getItem("idMembre")!=null){
        profil= <li className="nav-item"><Link className="nav-link" to="/profil">Mon profil</Link></li>
    }
    if(sessionStorage.getItem("idMembre")==null){
        profil= <></>
    }
       if(sessionStorage.getItem("idMembre")!=null){
           estConnete= <li className="nav-item"><a className="nav-link" onClick={this.deconnexion} style={{"cursor": "pointer"}}>Me déconnecter</a></li>
       }
       if(sessionStorage.getItem("idMembre")==null){
        estConnete= <li className="nav-item"><Link className="nav-link" to="/login" style={{"cursor": "pointer"}}>Connexion</Link></li>
       }
       if(sessionStorage.getItem("idMembre")!=''){
           nav=<nav className="d-flex aic">
           <Link to="/profil" className="login">
           <table>
               <tr>
                   <td><Avatar  style={{width:35,height:35}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photo}`}  /></td>
                   <td>&nbsp;{this.state.nom}</td>
               </tr>
               
           </table>
           
          </Link>
           <ul className="nav social d-none d-md-flex">
               <li style={{cursor:'pointer'}}>
                       <a><Badge  onClick={this.handleClick} badgeContent={this.state.notif} color="error">
                           <NotificationsIcon />
                       </Badge>
                       <Menu
                       id="simple-menu"
                       anchorEl={this.state.anchorEl}
                       keepMounted
                       open={Boolean(this.state.anchorEl)}
                       onClose={this.handleClose}
                   >
                        <Link className="nav-link" to="/profil">
                            <MenuItem onClick={this.handleClose}><ChatBubbleIcon/><span style={{fontSize:'13px',color:'gray',textTransform:'initial'}}>&nbsp;
                                {this.state.resultatValidation}&nbsp;<br/>&nbsp;Motif:&nbsp;"{this.state.motif}" {result}</span>
                            </MenuItem>
                        </Link>
                       
                      
                   </Menu>
                   </a>
               </li>
               <li><Link to="/favori"><FavoriteIcon style={{color:'#ff7086'}}/></Link></li>
               <li><a  href="https://www.facebook.com/antananarivouniversity/" target="_blank"><i  className="fa fa-facebook"></i></a></li>
               <li><a href="http://www.univ-antananarivo.mg/"><i className="fa fa-google-plus"></i></a></li>
               <li><a href="https://twitter.com/intent/tweet?text=.%3A%3A%20Universit%C3%A9%20d%27Antananarivo%20%3A%3A.&url=http%3A%2F%2Fwww.univ-antananarivo.mg%2FContact-93"><i className="fa fa-twitter"></i></a></li>
           </ul>
       </nav>
       }
       if(sessionStorage.getItem("idMembre")==null){
        nav=<nav className="d-flex aic">
       
        <ul className="nav social d-none d-md-flex">
        
            <li><a href="https://www.facebook.com/antananarivouniversity/" target="_blank"><i  className="fa fa-facebook"></i></a></li>
            <li><a href="http://www.univ-antananarivo.mg/"><i className="fa fa-google-plus"></i></a></li>
            <li><a href="https://twitter.com/intent/tweet?text=.%3A%3A%20Universit%C3%A9%20d%27Antananarivo%20%3A%3A.&url=http%3A%2F%2Fwww.univ-antananarivo.mg%2FContact-93"><i className="fa fa-twitter"></i></a></li>
        </ul>
    </nav>
       }
       if(this.state.datedemande!=null){
           result=<span style={{fontSize:'13px',color:'gray'}}>{Moment(this.state.datedemande).locale("de").format('Do MMMM YYYY')}</span>
       }
       if(this.state.datedemande==''){
        result=<>Aucune notification</>
       }

    return (
        <header className="position-absolute w-100" style={{ "position": "fixed","top": "0","width": "100%"}} >
        <div className="container" >
            <div className="top-header d-none d-sm-flex justify-content-between align-items-center">
                <div className="contact">
                    <a href="tel:+261 20 22 326 39" className="tel"><i className="fa fa-phone" aria-hidden="true"></i>+261 20 22 326 39</a>
                    <a href="mailto:info@univ-antananarivo.mg"><i className="fa fa-envelope"
                            aria-hidden="true"></i>info@univ-antananarivo.mg</a>
                </div>
                {nav}
            </div>
            <nav className="navbar navbar-expand-md navbar-light" >
                <Link className="navbar-brand" to="/">
                    <table>
                        <tr>
                            <td><img variant="rounded"  style={{width:35,height:35,borderRadius:'3px'}} src="assets/images/iserazo.png" alt="CEROM" /></td>
                            <td><img variant="rounded"  style={{width:35,height:35}} src="assets/images/logo-cercom.gif" alt="CEROM" /></td>
                            <td style={{fontFamily:'Bahnschrift SemiBold',fontSize:30}}>&nbsp;CERCOM</td>
                        </tr>
                    </table>
                </Link>
                <div className="group d-flex align-items-center">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"><span
                            className="navbar-toggler-icon"></span></button>
                    <a className="login-icon d-sm-none" href="#"><i className="fa fa-user"></i></a>
                  
                </div>
               
               
                <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul className="navbar-nav">
                        <li className="nav-item"><Link className="nav-link" to="/">Accueil</Link></li>
                        <li className="nav-item"><Link className="nav-link" to="/ouvrage">Ouvrages</Link></li>
                        {profil}
                        {progressionL}
                      
                        {estConnete}
                       
                        <li className="nav-item"><Link className="nav-link" to="/autre">Autre</Link></li>
                     
                    </ul>
                    <a href="mailto:info@univ-antananarivo.mg"><i className="fa fa-envelope"></i></a>
                </div>
            </nav>
        </div>
        </header>
    )
   }
}