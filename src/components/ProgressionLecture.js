import React, { Component }  from 'react';
import { Link } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Axios from 'axios';

export default class ProgressionLecture extends Component {
    constructor(props) {
        super(props)
        this.state={
            pglecture:[],
         
          };
        
        }
    componentDidMount(){
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/progressionlecture.php')
		.then(response=>{
		  this.setState({pglecture:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
    }
    render(){
    return(
        <section class="featured">
        <div class="container">
        <div className="row">
            <div className="col-sm-12 col-lg-12"><br/><br/><br/></div>
        </div>
            <div class="row">
                <div class="col-md-12" data-aos="fade-right" data-aos-delay="400" data-aos-duration="800">
                    <div class="title">
                        <h6 class="title-primary"></h6>
                        <h1 class="title-blue">Progression de lecture</h1>
                    </div>
                    <p>Vous trouverez ci-dessous les livres les plus lus dans notre bibliothèques par les membres dans ce
                        système.</p>
                        { this.state.pglecture.map(pgl=>
                        <div className="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                            <div className="media" data-aos="fade-up" data-aos-delay="200" data-aos-duration="400">
                                <div className="media-body" style={{"textAlign":"justify"}}>
                                    <table>
                                        <tr>
                                            <td>
                                                <img  className="mr-4" src={decodeURIComponent( decodeURIComponent(pgl.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} width="50px" style={{"borderRadius":"5px"}}/>
                                             
                                            </td>
                                            <td>
                                                <h5>
                                                    <Link style={{"color":"#1d1e52","display": "inline"}} to="/detailOuvrage">
                                                    {pgl.titre} ({pgl.nomAuteur})
                                                    </Link>
                                                    <h6 style={{"fontFamily":"Arial","fontSize":"13px","color":"#4d4b4b","text-transform":"uppercase"}}>Nombre de vus:</h6>
                                                    <Box width="10%" style={{"backgroundColor":"#467a33","color":"white"}} p={1} my={0.5}>{pgl.nombreVu}</Box>
                                                   
                                                </h5>
                                                <Divider />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        )}
                </div>
               
            </div>
        </div>
    </section>
    )
}
}